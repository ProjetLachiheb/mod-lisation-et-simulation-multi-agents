globals [maxMatch hiredNb firedNb quitNb un va]

breed [persons person]
breed [companies company]

;; state -> 0 ou 1 (repres. vacant/filled pour company ou unemployed/employed pour person)
;; skill -> seq de 5 boolean valeurs
;; salary -> repres. salaire pour person et salaire min pour company
turtles-own [state skill salary]

;;productivity -> entre 0 et 1 repres. productivite exercer au travail
;;productivity -> entre 0 et 1 repres. satisfaction par rapport au travail
persons-own [productivity satisfaction]

to setup
  clear-all
  create-persons U
  create-companies V

  set hiredNb 0
  set firedNb 0
  set quitNb 0

  ask turtles [
    setxy random-pxcor random-pycor
    set state 0
    set skill (list (random 2) (random 2) (random 2) (random 2) (random 2) )
    set salary random-float 1
  ]
  ask persons [
    set color 109
    set productivity 0
    set satisfaction 0
  ]
  ask companies [
   set color red
  ]
  ask patches [set pcolor 139.9]
  ;; Initialisation du pourcentage de matching en fonction du nombre poste vacant et de chomeur :
  ifelse (U < V)
  [set maxMatch (U) * percent-matching]
  [set maxMatch (V) * percent-matching]
  set un (ifelse-value (count persons with [state = 0] = 0) [0] [((count persons with [state = 0]) / (count persons))])
  set va (ifelse-value ( (count companies with [state = 0] = 0) or (count persons = 0) ) [0] [( (count companies with [state = 0]) / (count persons) ) ] )
  reset-ticks

end

;;Setup special pour run-multiple
;;Ce setup est necessaire comme on se sert pas des sliders U et V pour celui-ci et qu'il faut mettre a jour les plots d'une maniere differente
to setup-run-mult [ unem vac ]
  clear-turtles
  clear-links
  clear-patches
  clear-globals

  create-persons unem
  create-companies vac

  set hiredNb 0
  set firedNb 0

  ask turtles [
    setxy random-pxcor random-pycor
    set state 0
    set skill (list (random 2) (random 2) (random 2) (random 2) (random 2) )
    set salary random-float 1
  ]
  ask persons [
    set color blue
    set productivity 0
    set satisfaction 0
  ]
  ask companies [
   set color red
  ]
  ask patches [set pcolor 139.9]
  ;; Initialisation du pourcentage de matching en fonction du nombre poste vacant et de chomeur :
  ifelse (unem < vac)
  [set maxMatch (unem) * percent-matching]
  [set maxMatch (vac) * percent-matching]


end

;; 45 est la valeur jaune c'est la couleur quand la productivite est nule
;; Plus la productivite augmente plus le jaune devient sombre (on augmente de 0.3 la valeur de la couleur pour chaque 0.1 de productivite )
to-report level-color-productivity [prod]
  report ( 45 - (prod * 3) )
end

;; Meme logique comme pour la couleur de la productivite
to-report level-color-satisfaction [sat]
  report ( 109 - (sat * 3) )
end


;; Verifie si productivite de tout les agents person > 0.5 + rompt lien entre agent person et company si < 0.5
to check-prod
  ask persons with [state = 1] [
    if ((productivity < 0.5) or (random-float 1 < licenciement-innatendu)) [
      ask my-links [
        ask other-end [set state 0]
        set firedNb (firedNb + 1)
        die
      ]
      set state 0

    ]
  ]
end

;; Meme logique comme pour la productivite, le lien se rompt si la satisfaction est < 0.5
to check-sat
  ask persons with [state = 1] [
    if (satisfaction < 0.5) [
      ask my-links [
        ask other-end [set state 0]
        set quitNb (quitNb + 1)
        die
      ]
      set state 0

    ]
  ]
end


;; Fait varier la productivite des agents d'une valeur tiré entre 0 et max-prod-fluc pour chaque agent
to update-productivity
  ask persons [
    let upd (random-float max-prod-fluc)
    ifelse (random-float 1) > 0.5 [set productivity (productivity - upd)] [set productivity (productivity + upd)]

    if productivity < 0
    [
        set productivity 0
    ]
    if productivity > 1
    [
        set productivity 1
    ]

    ask my-links [
      set color level-color-productivity [productivity] of myself ;;la couleur du lien correspond a la productivite
    ]

  ]
end

;; Exactement la meme logique que pour update-productivity mais avec une valeur tire entre 0 et max-sat-fluc
to update-satisfaction
  ask persons [
    let upd (random-float max-sat-fluc)
    ifelse (random-float 1) > 0.5 [set satisfaction (satisfaction - upd)] [set satisfaction (satisfaction + upd)]

    if satisfaction < 0
    [
        set satisfaction 0
    ]
    if satisfaction > 1
    [
        set satisfaction 1
    ]


    set color level-color-satisfaction satisfaction ;; la couleur de l'agent correspond a sa satisfaction


  ]
end



;; --------------------------------------------------------------------------------Calcul similarites------------------------------------------------------------------------------------

;; renvoi la valeur normalise de la comparaison des skill
;; (skill posede par un agent person et skill requis par l'agent company)
to-report compare-skill [sk1 sk2]
  let s reduce + ( map [ [a b] -> ifelse-value a = b [1] [0] ] sk1 sk2 )
  report s / 5.0
end

;; calc similarite des salaires
;; (comme le salaire est deja normalise a une valeur [0,1] il suffit de soustraire la difference des salaires a 1 pour obtenir la similarite)
to-report compare-salary [sal1 sal2]
  report 1 - (abs (sal1 - sal2))
end

;; Calcul de la similarite (cas de base ou la similarite est symetrique)
to-report calc-sim [pers comp]
  let sk (compare-skill [skill] of pers [skill] of comp)
  let l (ifelse-value (member? comp (turtles-on pers)) [1] [0])
  let sa (compare-salary [salary] of pers [salary] of comp)
  let sim (sk + l + sa) / 3.0

  report sim
end

;; Calcul de la similarite du cote de la personne (pour l'extension Q 2.3)
to-report calc-sim-comp [pers comp]
  let sk (compare-skill [skill] of pers [skill] of comp)
  let l (ifelse-value (member? comp (turtles-on pers)) [1] [0])
  let sa (compare-salary [salary] of pers [salary] of comp)
  ;;let sim (sk * 0.55) + (l * 0.15) + (sa * 0.3)
  let sim (sk * skill-coef-comp) + (l * loc-coef-comp) + (sa * sal-coef-comp)

  report sim
end

;; Calcul de la similarite du cote de l'entreprise (pour l'extension Q 2.3)
to-report calc-sim-pers [pers comp]
  let sk (compare-skill [skill] of pers [skill] of comp)
  let l (ifelse-value (member? comp (turtles-on pers)) [1] [0])
  let sa (compare-salary [salary] of pers [salary] of comp)
  let sim (sk * skill-coef-pers) + (l * loc-coef-pers) + (sa * sal-coef-pers)

  report sim
end

;; Calcul similarite avec les fonctions calc-sim (differente fonction selon si l'extension est allume) + creation de lien si celle-ci > 0.5
to compute-similarities [pers comp]

  let sim-p (ifelse-value extension-pt2? [calc-sim-pers pers comp] [calc-sim pers comp])
  let sim-c (ifelse-value extension-pt2? [calc-sim-comp pers comp] [calc-sim pers comp])


  ;; Application de la motivation innatendue de l'entreprise  + Borne sup:
  if (random-float 1 < motiv-innat-comp) [
    set sim-c (sim-c + percent-increaseC)
    if not extension-pt2? [set sim-p (sim-p + percent-increaseC)]


  ]

  ;; Application de la motivation innatendue de la personne  + Borne sup:
  if (random-float 1 < motiv-innat-pers) [
    set sim-p (sim-p + percent-increaseP)
    if not extension-pt2? [set sim-c (sim-c + percent-increaseP)]


  ]

  ;;Borne sup
  if (sim-c > 1) [set sim-c 1]
  if (sim-p > 1) [set sim-p 1]


  ;; Test de similarity ;
  if sim-p > 0.5 and sim-c > 0.5 [
    ask pers [
      ;; Creation d'un lien d'embauche entre pers et comp :
      create-link-with comp
      set hiredNb (hiredNb + 1) ;;incrementer nombre personnes embauche

      ;; La productivite et la satisfaction sont intialises a la valeur de la similarite
      set productivity sim-p
      set satisfaction sim-p

      ask my-links [
        set color level-color-productivity [productivity] of myself
      ]
      ;; L'agent Person pers a un poste :
      set state 1
      ask comp [
       ;; Le poste de company est maintant occupé :
       set state 1
      ]
    ]
  ]
end



;;------------------------------------------------------------------------------------Matching------------------------------------------------------------------------------------
;; Fonction de matching -> check productivite + essai de matcher match-nb paires + mise a jour productivite
to matching
  ;; Remettre a 0 compteurs pour le nb de gens embauches , le nb de gens licencies et le nb de gens qui ont demissione (utilise que dans l'extension)
  set hiredNb 0
  set firedNb 0
  set quitNb 0
  let nbUnempl count (persons with [state = 0])
  let nbVac count (companies with [state = 0])

  ;; Verification de la productivite + la satisfaction (si l'extension est allume)
  check-prod
  if extension-pt1? [check-sat]

  ;; Matching
  let match-nb maxMatch
  if match-nb > (min (list nbUnempl nbVac)) [set match-nb (min (list nbUnempl nbVac)) ] ;;check si match-nb n'est pas plus grand que ce dont on dispose
  if  (nbUnempl > 0) and (nbVac > 0) [
      foreach (range match-nb) [compute-similarities one-of persons with [state = 0] one-of companies with [state = 0]]
  ]

  ;; Mise a jour de la productivite + la satisfaction (si l'extension est allume)
  update-productivity
  if extension-pt1? [update-satisfaction]

end

;;---------------------------------------------------------------------------------Run Simulation--------------------------------------------------------------------------------

;; Faire tourner un tour de simulation
to go

  matching
  ;; un ->  taux de chomage et va -> taux de vacance
  set un (ifelse-value (count persons with [state = 0] = 0) [0] [((count persons with [state = 0]) / (count persons))])
  set va (ifelse-value ( (count companies with [state = 0] = 0) or (count persons = 0) ) [0] [( (count companies with [state = 0]) / (count persons) ) ] )
  tick

end

;; Faire tourner nb fois la simulation ( utilise pour les tests de sensibilite aux parametres )
to run-nb [nb]

  foreach (range nb) [
    matching
    ;; un ->  taux de chomage et va -> taux de vacance
    set un (ifelse-value (count persons with [state = 0] = 0) [0] [((count persons with [state = 0]) / (count persons))])
    set va (ifelse-value ( (count companies with [state = 0] = 0) or (count persons = 0) ) [0] [( (count companies with [state = 0]) / (count persons) ) ] )
    tick
  ]

  show (word "Simulation with U=" U "V=" V " :"  )
  show (word "Unemployement rate : " ((count persons with [state = 0]) / (count persons )) "  Vacancy rate : " ((count companies with [state = 0]) / (count persons)) )
  ;;show (word "Similarity symetry: " (not extension-pt2?) )
  ;;show ( word "Person coefs:  skill: " skill-coef-pers " location: " loc-coef-pers " salary: " sal-coef-pers)
  ;;show ( word "Company coefs:  skill: " skill-coef-comp " location: " loc-coef-comp " salary: " sal-coef-comp)
  ;;show (word "Percent matching :" percent-matching)
  ;;show (word "Unexpected firing :" licenciement-innatendu)
  ;;show (word "Unexpected company motivation :" motiv-innat-comp)
  ;;show (word "Raise value :" percent-increaseC)
  ;;show (word "Max productivity fluctuation: " max-prod-fluc)
  ;;show (word "Workers quitting: " extension-pt1?)


  show ( "--------------------------------------------------------------------")

end

;; Fait tourner 16 simulations a la suite ( pour toute valeurs de U et V possible)
to run-multiple

  clear-all

  foreach (range 100 500 100) [ x ->
    foreach (range 100 500 100) [ y ->

      setup-run-mult x y
      foreach (range 1000) [
         matching
      ]
      show (word "Simulation with U=" x "V=" y " :"  )
      show (word "Unemployement rate : " ((count persons with [state = 0]) / (count persons )) "  Vacancy rate : " ((count companies with [state = 0]) / (count persons)) )

      ;; un ->  taux de chomage et va -> taux de vacance
      set un (ifelse-value (count persons with [state = 0] = 0) [0] [((count persons with [state = 0]) / (count persons))])
      set va (ifelse-value ( (count companies with [state = 0] = 0) or (count persons = 0) ) [0] [( (count companies with [state = 0]) / (count persons) ) ] )

      ;; reset-ticks doit etre ici car on fait appel a setup-run-mult plusieurs fois et on ne veut pas reset les ticks
      if ( x = 100 and y = 100) [reset-ticks] ;; reset-ticks n'est pas fait avant car on veut commencer a afficher les points une fois que la premiere simulation est finie et non avant

      tick

    ]
  ]

end
@#$#@#$#@
GRAPHICS-WINDOW
466
10
932
477
-1
-1
13.9
1
10
1
1
1
0
1
1
1
-16
16
-16
16
0
0
1
ticks
30.0

SLIDER
7
14
179
47
U
U
100
400
100.0
100
1
NIL
HORIZONTAL

SLIDER
7
61
179
94
V
V
100
400
200.0
100
1
NIL
HORIZONTAL

BUTTON
11
242
85
275
Setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
100
242
165
275
Step
matching
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
195
153
375
186
motiv-innat-comp
motiv-innat-comp
0
1
0.1
0.1
1
NIL
HORIZONTAL

SLIDER
194
13
379
46
percent-matching
percent-matching
0
1
0.25
0.05
1
NIL
HORIZONTAL

SLIDER
195
108
384
141
percent-increaseC
percent-increaseC
0
1
0.2
0.1
1
NIL
HORIZONTAL

SLIDER
196
61
422
94
licenciement-innatendu
licenciement-innatendu
0
1
0.1
0.1
1
NIL
HORIZONTAL

MONITOR
333
232
427
277
nb unemployed
count (persons with [state = 0])
17
1
11

MONITOR
256
232
323
277
nb vacant
count (companies with [state = 0])
17
1
11

BUTTON
179
242
242
275
Go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
11
458
211
608
Vacancy rate
time
vacancy rate
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot ifelse-value (count companies with [state = 0] = 0) [0] [( (count companies with [state = 0]) / (count persons) )]"

PLOT
229
290
429
440
Hiring rate
time
hiring rate
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot ifelse-value (count persons with [state = 0] = 0) [0] [(hiredNb / (count persons with [state = 0]))]"

PLOT
229
458
429
608
Firing rate
time
firing rate
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot (ifelse-value (count persons with [state = 1] = 0) [0] [(firedNb / (count persons with [state = 1]))])"

PLOT
11
289
211
439
Unemployement rate
time
unemployement rate
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot ifelse-value (count persons with [state = 0] = 0) [0] [( (count persons with [state = 0]) / (count persons) )]"

SLIDER
8
108
180
141
percent-increaseP
percent-increaseP
0
1
0.2
0.1
1
NIL
HORIZONTAL

SLIDER
8
154
180
187
motiv-innat-pers
motiv-innat-pers
0
1
0.1
0.1
1
NIL
HORIZONTAL

BUTTON
19
665
129
698
NIL
run-multiple
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
9
197
181
230
max-prod-fluc
max-prod-fluc
0
1
0.3
0.1
1
NIL
HORIZONTAL

PLOT
967
10
1419
264
Beveridge curve
U
V
0.5
1.0
0.0
4.0
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "plotxy un va"

SLIDER
1146
350
1318
383
max-sat-fluc
max-sat-fluc
0
1
0.4
0.1
1
NIL
HORIZONTAL

PLOT
981
396
1296
516
Quit rate
NIL
NIL
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot ifelse-value (count persons with [state = 1] = 0) [0] [(quitNb / (count persons with [state = 1]))]"

SWITCH
981
350
1132
383
extension-pt1?
extension-pt1?
1
1
-1000

SWITCH
987
609
1138
642
extension-pt2?
extension-pt2?
1
1
-1000

BUTTON
207
193
358
226
run 2000 iterations
run-nb 2000
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
982
320
1284
350
Extension question 2.1 additional parameters :
12
0.0
1

TEXTBOX
986
580
1259
610
Extension question 2.3 additional parameters :
12
0.0
1

TEXTBOX
20
640
320
685
Run for all values of U and V (16 different setups) :
12
0.0
1

SLIDER
986
715
1158
748
skill-coef-pers
skill-coef-pers
0
1
0.8
0.05
1
NIL
HORIZONTAL

SLIDER
986
767
1158
800
loc-coef-pers
loc-coef-pers
0
1
0.1
0.05
1
NIL
HORIZONTAL

SLIDER
987
664
1159
697
sal-coef-pers
sal-coef-pers
0
1
0.1
0.05
1
NIL
HORIZONTAL

SLIDER
1182
664
1354
697
sal-coef-comp
sal-coef-comp
0
1
0.1
0.05
1
NIL
HORIZONTAL

SLIDER
1182
715
1354
748
skill-coef-comp
skill-coef-comp
0
1
0.8
0.05
1
NIL
HORIZONTAL

SLIDER
1182
766
1354
799
loc-coef-comp
loc-coef-comp
0
1
0.1
0.05
1
NIL
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

Simple version of the labor market

## HOW IT WORKS

Two types of agents : Person (blue) and Comapny (red)

At each simulation step a fixed proportion of pairs is considered, each of these pairs try to be matched with one another based on these criteria : salary, skill and location.


## HOW TO USE IT

All sliders in the top left corner of the simulation window are the main parameters for this simulation. 

All other sliders are part of the extensions to this model and to be used in the simulation the corresponding switches (either extenstion-pt1? or extension-pt2?) need to be turned on.

When running a single simulation, you first need to click on the button setup and then use either the button step (runs one iteration), go or run 2000 iterations.

Run-multiple does not require any setup as it has it's own special setup already included, however this is the only button that does not make use of the U and V sliders (althought still uses all the other main parameters) in it's setup, this is because run-multiple runs one by one 16 different configurations where only U and V vary.
 

## THINGS TO NOTICE



## THINGS TO TRY



## EXTENDING THE MODEL



## NETLOGO FEATURES


## RELATED MODELS



## CREDITS AND REFERENCES
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.1.1
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 1 1.0 0.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
